package com.company.model;

import com.company.controller.Card;
import com.company.controller.Player;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StateOfGame
{
    private Long id;
    private boolean isGameContinue;
    private int answerOnChoose;
    private HashMap<Player, Card> positionsOfPlayers;
    private HashMap<Integer, Card> positionOfCards;

    private static int yesAnswer = 0;
    private static int noAnswer = 1;

    public StateOfGame(List<Player> players, List<Card> cards)
    {
        positionOfCards = new HashMap<>();
        for (Card card : cards)
        {
            positionOfCards.put(card.getIdCard(), card);
        }

        positionsOfPlayers = new HashMap<>();
        for (Player player : players)
        {
            positionsOfPlayers.put(player, positionOfCards.get(0));
        }
    }

    public StateOfGame(List<Player> players, List<Card> cards, List<Card> playerPosition)
    {
        positionOfCards = new HashMap<>();
        for (Card card : cards)
        {
            positionOfCards.put(card.getIdCard(), card);
        }

        positionsOfPlayers = new HashMap<>();
        int i = 0;

        for (Player player : players)
        {
            positionsOfPlayers.put(player, playerPosition.get(i));
            i++;

        }
    }

    public boolean getIsGameContinue()
    {
        return isGameContinue;
    }

    public Map<Integer, Card> getPositionOfCards()
    {
        return positionOfCards;
    }

    public boolean getAnswerOnChoose()
    {

        return answerOnChoose == 0;
    }

    public Map<Player, Card> getPositionsOfPlayers()
    {
        return positionsOfPlayers;
    }

    public StateOfGame setIsGameContinue(boolean isGameContinue)
    {
        this.isGameContinue = isGameContinue;
        return this;
    }

    public StateOfGame setAnswerOnChoose(int answerOnChoose)
    {
        this.answerOnChoose = answerOnChoose;
        return this;
    }

    public StateOfGame setAnswerOnChooseForBot(Player player)
    {

        if (player.getMoney() * 0.8 > positionsOfPlayers.get(player).getBuilding().getCost())
            this.answerOnChoose = yesAnswer;
        else
            this.answerOnChoose = noAnswer;

        return this;
    }
}
