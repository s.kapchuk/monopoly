package com.company.model;

import com.company.controller.Card;
import com.company.controller.Player;
import com.company.controller.PlayerCard;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StateOfGameEntity
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToMany(mappedBy = "stateOfGameEntity", fetch = FetchType.LAZY)
    private List<Player> players = new ArrayList<>();

    @OneToMany(mappedBy = "stateOfGameEntity",fetch = FetchType.LAZY)
    private List<Card> cards = new ArrayList<>();

    @OneToMany(mappedBy = "stateOfGameEntity",fetch = FetchType.LAZY)
    private List<PlayerCard> playerCards = new ArrayList<>();

    public List<Card> getCards()
    {
        return cards;
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public List<PlayerCard> getPlayerCards()
    {
        return playerCards;
    }

    public void addPlayer(Player player)
    {
        players.add(player);
    }

    public void addCard(Card card)
    {
        cards.add(card);
    }

    public void addPlayerCards(PlayerCard playerCard)
    {
        playerCards.add(playerCard);
    }

    public long getId()
    {
        return id;
    }
}
