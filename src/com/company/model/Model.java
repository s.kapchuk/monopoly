package com.company.model;

import com.company.controller.Building;
import com.company.controller.BuildingType;
import com.company.controller.Card;
import com.company.controller.Player;

import java.util.*;

public class Model
{
    private static int numberOfCards = 20;
    private static int startBonus = 3000;
    private static int plusMinusCardBonus = 500;
    private static int prisonPosition = 5;
    private static int lotteryBonus = 1000;

    private Random random;
    private StateOfGame stateOfGame;

    public Model(List<Player> players, List<Card> cards)
    {
        stateOfGame = new StateOfGame(players, cards);
        random = new Random();
    }

    public StateOfGame buyBuilding(Player player, boolean answer)
    {
        Card card = stateOfGame.getPositionsOfPlayers().get(player);
        Building building = card.getBuilding();
        if (building.getBuildingType() == BuildingType.BUSINESS && answer)
        {
            payMoneyForBuilding(building, player);
        }
        return isGameContinue();
    }

    public boolean shouldMakeBuyingChoose(Player player)
    {
        Card card = stateOfGame.getPositionsOfPlayers().get(player);

        if (card.getBuilding().getBuildingType() == BuildingType.BUSINESS && card.getBuilding().getOwner() != null)
        {
            payMoneyPlayer(card, player);
            return false;
        }
        else
            return card.getBuilding().getBuildingType() == BuildingType.BUSINESS;
    }

    public boolean shouldMakeLotteryChoose(Player player)
    {
        Card card = stateOfGame.getPositionsOfPlayers().get(player);
        return card.isHasBuilding() && card.getBuilding().getBuildingType() == BuildingType.LOTTERY;
    }

    public StateOfGame tryLottery(Player player, int result)
    {
        if (result > 4)
        {
            player.setMoney(player.getMoney() + lotteryBonus);
        }
        else
            player.setMoney(player.getMoney() - lotteryBonus);

        return isGameContinue();
    }

    public StateOfGame isGameContinue()
    {
        Player player;
        int numberOfPlayersWithoutMoney = stateOfGame.getPositionsOfPlayers().size();

        for (Map.Entry<Player, Card> entry : stateOfGame.getPositionsOfPlayers().entrySet())
        {
            player = entry.getKey();
            if (player.getMoney() <= 0)
            {
                numberOfPlayersWithoutMoney--;
            }
        }

        boolean result = true;
        if (numberOfPlayersWithoutMoney <= 1)
           result = false;

        stateOfGame.setIsGameContinue(result);
        return stateOfGame;
    }

    public int throwDice()
    {
        return random.nextInt(5) + 1;
    }

    private void payMoneyPlayer(Card card, Player player)
    {
        int resultMoney = player.getMoney() - card.getBuilding().getTax();
        if (resultMoney < 0)
            resultMoney = 0;

        player.setMoney(resultMoney);
        card.getBuilding().getOwner().setMoney(card.getBuilding().getOwner().getMoney() + card.getBuilding().getTax());
    }

    private void payMoneyForBuilding(Building building, Player player)
    {
        if (player.getMoney() - building.getCost() > 0)
        {
            player.setMoney(player.getMoney() - building.getCost());
            building.setOwner(player);
        }
    }

    public int newPositionOFPlayer(Player player, int numberOnDice)
    {
        Card card = stateOfGame.getPositionsOfPlayers().get(player);
        Card newPosition = stateOfGame.getPositionOfCards().get((card.getIdCard() + numberOnDice) % numberOfCards);

        if (newPosition.isHasBuilding() && newPosition.getBuilding().getBuildingType() == BuildingType.START)
        {
            player.setMoney(player.getMoney() + startBonus);
        }
        else
            if (newPosition.isHasBuilding() && newPosition.getBuilding().getBuildingType() == BuildingType.POLICE)
            {
            newPosition = stateOfGame.getPositionOfCards().get(prisonPosition);
        }
        else if (newPosition.isHasBuilding() && newPosition.getBuilding().getBuildingType() == BuildingType.MINUS)
        {
            player.setMoney(player.getMoney() - plusMinusCardBonus);
        }
        else if (newPosition.isHasBuilding() && newPosition.getBuilding().getBuildingType() == BuildingType.PLUS)
        {
            player.setMoney(player.getMoney() + plusMinusCardBonus);
        }

        stateOfGame.getPositionsOfPlayers().put(player, newPosition);
        return newPosition.getIdCard();
    }

    public StateOfGame getStateOfGame()
    {
        return stateOfGame;
    }

    public void setStateOfGame(StateOfGame stateOfGame)
    {
        this.stateOfGame = stateOfGame;
    }
}
