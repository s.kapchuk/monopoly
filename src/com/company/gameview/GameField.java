package com.company.gameview;

import com.company.controller.Card;
import com.company.controller.Controller;
import com.company.controller.ImageRepository;
import com.company.controller.Player;
import com.company.gameview.listeners.ShowMenuListener;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GameField extends JFrame
{
    private ArrayList<Player> players;
    private ArrayList<Card> cards;
    private JButton saveGame;

    private PlayerPanel playerPanel;
    private GamePanel gamePanel;

    private ImageRepository imageRepository;

    public GameField(List<Player> players, List<Card> cards, Controller controller, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        this.players = new ArrayList<>(players);
        this.cards = new ArrayList<>(cards);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setExtendedState(MAXIMIZED_BOTH);
        setResizable(false);
        setLayout(null);

        setVisible(true);
        addPlayerPanel();
        addGamePanel();
        addSaveButton();
        saveGame.addMouseListener(new ShowMenuListener(controller));

        repaint();
    }

    private void addSaveButton()
    {
        int partOfFrame = 4;
        saveGame = new JButton("QUIT THE GAME");
        saveGame.setBounds( getWidth() / partOfFrame*3, 0, getWidth() / partOfFrame,  getHeight()/12);
        saveGame.setVisible(true);
        saveGame.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 20));
        saveGame.setBackground(Color.PINK);
        add(saveGame);
        saveGame.repaint();
    }

    private void addPlayerPanel()
    {
        int partOfFrame = 4;
        int widthPlayerPanel = getWidth() / partOfFrame;

        playerPanel = new PlayerPanel(widthPlayerPanel, getHeight(), players, imageRepository);
        add(playerPanel);
    }

    private void addGamePanel()
    {
        setVisible(true);
        int factorForGamePanel = 3;
        int partOfFrame = 4;

        int widthGamePanel = (getWidth()/partOfFrame) * factorForGamePanel;

        gamePanel = new GamePanel(widthGamePanel, getHeight(), cards, players, imageRepository);
        add(gamePanel);
    }

    @Override
    public void repaint()
    {
        gamePanel.repaint();
        playerPanel.repaint();
    }

    public void showMoveOfPlayer(Player lastMovedPlayer, int idOfCard)
    {
        gamePanel.setMove(lastMovedPlayer, idOfCard);

        while (gamePanel.getMove() != -1)
        {
            this.repaint();
        }
    }

    public void showMakeTurn(int turnResult)
    {
        TurnFrame turnFrame = new TurnFrame(turnResult, imageRepository);
        turnFrame.setBounds(getSize().width/4, getSize().height/4, 400, 500);
        turnFrame.setVisible(true);

            try
            {
             while (!turnFrame.getResultShown())
                Thread.sleep(1000);

                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
            }


        turnFrame.dispose();
    }

    public int showMakeBuyBuildingChoose()
    {
        return JOptionPane.showConfirmDialog(this, "Хотите приобрести данную недвижимость?", "Покупка",
                JOptionPane.YES_NO_OPTION);
    }

    public int showMakeLotteryChoose()
    {
        return JOptionPane.showConfirmDialog(this, "Хотите Сыграть в лоттерею?", "Лоттерея",
                JOptionPane.YES_NO_OPTION);
    }
}