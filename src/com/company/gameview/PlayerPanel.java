package com.company.gameview;

import com.company.controller.ImageRepository;
import com.company.controller.Player;
import com.company.gameview.icons.Coordinates;
import com.company.gameview.icons.IconPlayer;
import com.company.gameview.icons.TypeOfIcon;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class PlayerPanel extends JPanel
{
    private Image backGround;
    private ArrayList<IconPlayer> iconsPlayer = new ArrayList<>();
    private ImageRepository imageRepository;

    PlayerPanel(int width, int height, ArrayList<Player> players, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        this.setBackGround(width, height);
        int stepYY = 6;

        for (int i = 0; i < players.size(); i++)
        {
            iconsPlayer.add(new IconPlayer(imageRepository.getImage(0, TypeOfIcon.IconPlayer).getImage())
                    .setCoordinates(new Coordinates(0, height/stepYY*i + 20))
                    .setPlayer(players.get(i)));
        }
    }

    private void setBackGround(int width, int height)
    {
        setBounds(width*3, height/12, width, height);
        backGround = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.Money).getImage()).getImage();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        g.drawImage(backGround, 0, 0, getSize().width, getSize().height, null);

        int widthOfIcon = getSize().width/4;
        int heightOfIcon = getSize().height/6-50;

        for (IconPlayer icon : iconsPlayer)
        {

            g.drawImage(icon.getImageOfPlayer(), icon.getCoordinates().getX(),
                    icon.getCoordinates().getY(), widthOfIcon, heightOfIcon, null);

            g.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 20));

            int nameX = icon.getCoordinates().getX() + 200;
            int nameY = icon.getCoordinates().getY() + 50;
            g.drawString(icon.getPlayer().getName(), nameX, nameY);

            int moneyY = icon.getCoordinates().getY() + 100;
            int moneyX = icon.getCoordinates().getX() + 200;
            g.drawString(String.valueOf(icon.getPlayer().getMoney()), moneyX, moneyY);
        }
    }
}
