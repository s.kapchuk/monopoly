package com.company.gameview;


import com.company.controller.ImageRepository;
import com.company.gameview.icons.TypeOfIcon;
import com.company.gameview.listeners.Listener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class TurnFrame extends JFrame
{
    private JButton makeTurn;
    private JLabel resultOfTurn;
    private final JLabel result;
    private ImageRepository imageRepository;


    private boolean resultShown = false;

    TurnFrame(int turnResult, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        configuration();

        ImageIcon icon = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.ButtonMakeTurn).getImage());

        makeTurn = new JButton(" MAKE TURN ", icon);

        makeTurn.addMouseListener(new ResultShownListener());

        GridBagConstraints c = GridConstraint.getConstraint();

        resultOfTurn = new JLabel(new ImageIcon(imageRepository.getImage(0, TypeOfIcon.Dice).getImage()));
        resultOfTurn.setMinimumSize(new Dimension(300, 280));
        resultOfTurn.setPreferredSize(new Dimension(300, 280));
        resultOfTurn.setVisible(false);
        add(resultOfTurn, c);

        result = new JLabel("RESULT " + turnResult);
        result.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 20));
        result.setVisible(false);
        add(result, c);
        c.fill = GridBagConstraints.NONE;
        configureButton(makeTurn);
        add(makeTurn, c);
        pack();
    }

    public boolean getResultShown()
    {
        return resultShown;
    }

    private JButton configureButton(JButton button)
    {
        button.setVerticalTextPosition(SwingConstants.CENTER);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 15));
        button.setOpaque(false);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        return button;
    }

    private void configuration()
    {
        setLayout(new GridBagLayout());
        setResizable(false);
    }

    class ResultShownListener extends Listener
    {
        private boolean firstTime = true;

        ResultShownListener()
        {
            super(null);
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            if(firstTime)
            {
                resultOfTurn.setVisible(true);
                makeTurn.setText("STOP!");
                firstTime = false;
                return;
            }

            resultOfTurn.setVisible(false);
            result.setVisible(true);
            resultShown = true;
        }
    }
}
