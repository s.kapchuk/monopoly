package com.company.gameview;

public class GamePanelSizes
{
    private int buildingIndentX;
    private int buildingIndentY;

    private int nameIndentX;
    private int nameIndentY;

    private int costIndentY;
    private int taxIndentY;
    private int ownerIndentY;

    private int chipWidth;
    private int chipHeight;
    private int chipIndentX;
    private int chipIndentY;

    private int cardWidth;
    private int cardHeight;
    private int cardBuildingWidth;
    private int cardBuildingHeight;
    private int recordIndentX;

    public GamePanelSizes(int widthFrame, int heightFrame, int numberOfCards)
    {
        cardWidth = widthFrame/numberOfCards;
        heightFrame -= 35;
        cardHeight = heightFrame/numberOfCards;

        cardBuildingWidth = (int) ((double)widthFrame/numberOfCards*0.6);
        cardBuildingHeight = (int) ((double)heightFrame/numberOfCards*0.6);
        buildingIndentX = (int) (cardWidth*0.1);
        buildingIndentY = (int) (cardHeight*0.3);

        recordIndentX = (int) (cardBuildingWidth + cardBuildingWidth*0.2);
        nameIndentX = buildingIndentX;
        nameIndentY = cardHeight/6;

        int step = cardHeight/4;
        costIndentY = nameIndentY*2;
        taxIndentY = costIndentY + step;
        ownerIndentY = taxIndentY + step;

        chipWidth = widthFrame/numberOfCards/3;
        chipHeight = widthFrame/numberOfCards/3;
        chipIndentX =  widthFrame/numberOfCards/3;
        chipIndentY = heightFrame/numberOfCards/3;
    }

    public int getNameIndentX()
    {
        return nameIndentX;
    }

    public int getNameIndentY()
    {
        return nameIndentY;
    }

    public int getCostIndentY()
    {
        return costIndentY;
    }

    public int getTaxIndentY()
    {
        return taxIndentY;
    }

    public int getOwnerIndentY()
    {
        return ownerIndentY;
    }

    public int getChipHeight()
    {
        return chipHeight;
    }

    public int getChipWidth()
    {
        return chipWidth;
    }

    public int getChipIndentY()
    {
        return chipIndentY;
    }

    public int getChipIndentX()
    {
        return chipIndentX;
    }

    public int getCardWidth()
    {
        return cardWidth;
    }

    public int getCardHeight()
    {
        return cardHeight;
    }

    public int getCardBuildingWidth()
    {
        return cardBuildingWidth;
    }

    public int getCardBuildingHeight()
    {
        return cardBuildingHeight;
    }

    public int getRecordIndentX()
    {
        return recordIndentX;
    }

    public int getBuildingIndentX()
    {
        return buildingIndentX;
    }

    public int getBuildingIndentY()
    {
        return buildingIndentY;
    }
}
