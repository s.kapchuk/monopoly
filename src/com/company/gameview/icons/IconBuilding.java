package com.company.gameview.icons;

import com.company.controller.Building;
import javax.swing.*;
import java.awt.*;

public class IconBuilding
{
    private Image imageOfBuilding;
    private Building building;

    public IconBuilding(int number, byte[] image)
    {
        if (number < 0)
            throw new IllegalArgumentException();

        imageOfBuilding = new ImageIcon(image).getImage();
    }

    public IconBuilding setBuilding(Building building)
    {
        this.building = building;
        return this;
    }

    public Building getBuilding()
    {
        return building;
    }

    public Image getImageOfBuilding()
    {
        return imageOfBuilding;
    }
}
