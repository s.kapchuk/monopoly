package com.company.gameview.icons;

public enum TypeOfIcon
{
    IconBuilding, IconChip, IconGameCard, IconPlayer, Money, GrayButton, Dice, ButtonMakeTurn, Background, BackgroudMenu
}
