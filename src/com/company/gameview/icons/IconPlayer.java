package com.company.gameview.icons;

import com.company.controller.Player;
import javax.swing.*;
import java.awt.*;

public class IconPlayer extends JLabel
{
    private Image imageOfPlayer;
    private Coordinates coordinates;
    private Player player;

    public IconPlayer(byte[] image)
    {
        loadPhoto(image);
    }

    private void loadPhoto(byte[] image)
    {
        imageOfPlayer = new ImageIcon(image).getImage();
    }

    public Image getImageOfPlayer()
    {
        return imageOfPlayer;
    }

    public IconPlayer setPlayer(Player player)
    {
        if (player == null)
            throw new IllegalArgumentException("Player cant be empty for card");
        this.player = player;
        return this;
    }

    public Player getPlayer()
    {
        return player;
    }

    public Coordinates getCoordinates()
    {
        return coordinates;
    }

    public IconPlayer setCoordinates(Coordinates coordinates)
    {
        if (coordinates == null)
            throw new IllegalArgumentException("Coordinates cant be null");
        this.coordinates = coordinates;
        coordinates.updateX(20);
        return this;
    }
}
