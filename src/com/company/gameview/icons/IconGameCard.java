package com.company.gameview.icons;

import com.company.controller.BuildingType;
import com.company.controller.Card;

import javax.swing.*;
import java.awt.*;

public class IconGameCard
{
    private IconBuilding iconBuilding = null;
    private Image imageCard;
    private Coordinates coordinates;
    private Card card;

    public IconGameCard(byte[] image)
    {
        imageCard = new ImageIcon(image).getImage();
    }

    public IconGameCard setCard(Card card, byte[] image4, byte[] image2, byte[] image3,  byte[] imageByCardId)
    {
        if (card == null)
            throw new IllegalArgumentException();

        this.card = card;

        if (card.getBuilding().getBuildingType().equals(BuildingType.POLICE)
        || card.getBuilding().getBuildingType().equals(BuildingType.PRISON))
            imageCard = new ImageIcon(image4).getImage();

        if (card.getBuilding().getBuildingType().equals(BuildingType.BUSINESS))
            imageCard = new ImageIcon(image2).getImage();
        else
            imageCard = new ImageIcon(image3).getImage();

        if (card.isHasBuilding())
            iconBuilding = new IconBuilding(card.getIdCard(), imageByCardId).setBuilding(card.getBuilding());

        return this;
    }

    public Card getCard()
    {
        return card;
    }

    public Image getImageCard()
    {
        return imageCard;
    }

    public Coordinates getCoordinates()
    {
        return coordinates;
    }

    public IconGameCard setCoordinates(Coordinates coordinates)
    {
        if (coordinates == null)
            throw new IllegalArgumentException();
        this.coordinates = coordinates;
        return this;
    }

    public IconBuilding getIconBuilding()
    {
        return iconBuilding;
    }

    public boolean isHasIconBuilding()
    {
        return iconBuilding != null;
    }
}
