package com.company.gameview.icons;

import javax.swing.*;
import java.awt.*;

public class IconChip
{
    private Image imageOfChip;
    private Coordinates coordinates;
    private int idCard;

    public IconChip(int number, byte[] image)
    {
        if (number < 0)
            throw new IllegalArgumentException();

        imageOfChip = new ImageIcon(image).getImage();
    }

    public Image getImageOfChip()
    {
        return imageOfChip;
    }

    public void setCoordinates(Coordinates coordinates)
    {
        if (coordinates == null)
            throw new IllegalArgumentException();

        this.coordinates = coordinates;
    }

    public void setIdCard(int idCard)
    {
        this.idCard = idCard;
    }

    public int getIdCard()
    {
        return idCard;
    }

    public Coordinates getCoordinates()
    {
        return coordinates;
    }
}
