package com.company.gameview.icons;

public class Coordinates
{
    private int x;
    private int y;

    public Coordinates(int x, int y)
    {
        if (x < 0 || y < 0)
            throw new IllegalArgumentException("X and Y must be positive");

        this.x = x;
        this.y = y;
    }

    public Coordinates setX(int x)
    {
        if (x < 0)
            throw new IllegalArgumentException("X must be positive");
        this.x = x;
        return this;
    }

    public Coordinates setY(int y)
    {
        if (y < 0)
            throw new IllegalArgumentException("Y must be positive");
        this.y = y;
        return this;
    }

    public void updateX(int additive)
    {
        x += additive;
    }

    public void updateY(int additive)
    {
        y += additive;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
}

