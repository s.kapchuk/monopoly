package com.company.gameview;

import com.company.controller.BuildingType;
import com.company.controller.Card;
import com.company.controller.ImageRepository;
import com.company.controller.Player;
import com.company.gameview.icons.Coordinates;
import com.company.gameview.icons.IconChip;
import com.company.gameview.icons.IconGameCard;
import com.company.gameview.icons.TypeOfIcon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GamePanel extends JPanel
{
    private Image backGround;
    private ImageRepository imageRepository;


    private HashMap<Player, IconChip> playerChip;

    private ArrayList<IconGameCard> iconsGameCard;
    private ArrayList<String> cardLabels;
    private int move;

    private int curCard;
    private Player lastPlayer;
    private static final int NO_ACTION = -1;
    private int limitX = -1;
    private int limitY = -1;
    private int moveCount;

    private  GamePanelSizes gamePanelSizes;

    public GamePanel(int width, int height, ArrayList<Card> cards, ArrayList<Player> players, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        setBounds(0, 0, width, height);
        setBackGround();

        gamePanelSizes = new GamePanelSizes(width, height, cards.size()/4 + 1);
        iconsGameCard = new ArrayList<>();
        createIconsCard(cards);
        
        cardLabels = new ArrayList<>();
        cardLabels.add("Cost: ");
        cardLabels.add("Tax: ");
        cardLabels.add("Owner: ");

        playerChip = new HashMap<>();
        createIconsChips(players);

        move = NO_ACTION;
    }

    private void createIconsChips(ArrayList<Player> players)
    {
        for (int i = 0; i < players.size(); i++)
        {
            playerChip.put(players.get(i), new IconChip(i, imageRepository.getImage(i, TypeOfIcon.IconChip).getImage()));
        }

        int stepX = 0;
        for (Map.Entry<Player, IconChip> player : playerChip.entrySet())
        {
            this.playerChip.get(player.getKey()).setCoordinates(new Coordinates(iconsGameCard.get(0).getCoordinates().getX() +
                    gamePanelSizes.getChipIndentX() + stepX, iconsGameCard.get(0).getCoordinates().getY()
                    + gamePanelSizes.getChipIndentY()));
            stepX +=15;
        }
    }

    private void setBackGround()
    {
        backGround = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.Background).getImage()).getImage();
    }

    private void paintCards(Graphics g)
    {
        for (IconGameCard card : iconsGameCard)
        {
            g.drawImage(card.getImageCard(), card.getCoordinates().getX(),
                    card.getCoordinates().getY(), gamePanelSizes.getCardWidth(),
                    gamePanelSizes.getCardHeight(), null);

            if (card.isHasIconBuilding())
            {
                drawBuilding(g, card);
            }
        }
    }

    private void paintPlayers(Graphics g)
    {
        for (Map.Entry<Player, IconChip> player : playerChip.entrySet())
        {
            int height = gamePanelSizes.getChipHeight();
            if ((player.getKey()).equals(lastPlayer) && (moveCount < 40))
            {
                moveCount++;
            }
            else
            {
                if ((player.getKey()).equals(lastPlayer) && (moveCount >= 40))
                {
                    height -= 30;
                    moveCount++;
                    if (moveCount == 80)
                        moveCount = 0;
                }
            }

            g.drawImage(playerChip.get(player.getKey()).getImageOfChip(),
                    playerChip.get(player.getKey()).getCoordinates().getX() + gamePanelSizes.getChipIndentX(),
                    playerChip.get(player.getKey()).getCoordinates().getY() + gamePanelSizes.getChipIndentY(),
                    gamePanelSizes.getChipWidth(),
                    height, null);

            g.drawString(player.getKey().getName(),playerChip.get(player.getKey()).getCoordinates().getX() + gamePanelSizes.getChipIndentX(),
                    playerChip.get(player.getKey()).getCoordinates().getY() + gamePanelSizes.getChipIndentY());
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        g.drawImage(backGround, 0, 0, getSize().width, getSize().height, null);

        paintCards(g);

        if (move != NO_ACTION)
        {

            if (limitX > 0 || limitY > 0)
            {
                if (limitY > 0)
                {
                    if (iconsGameCard.get(curCard).getCoordinates().getY() > playerChip.get(lastPlayer).getCoordinates().getY())
                        playerChip.get(lastPlayer).getCoordinates().updateY(4);
                    else
                        playerChip.get(lastPlayer).getCoordinates().updateY(-4);

                    limitY -=4;
                }
                else
                {
                    if (iconsGameCard.get(curCard).getCoordinates().getX() > playerChip.get(lastPlayer).getCoordinates().getX())
                        playerChip.get(lastPlayer).getCoordinates().updateX(4);
                    else
                        playerChip.get(lastPlayer).getCoordinates().updateX(-4);

                    limitX -= 4;
                }
            }
            else {
                    if (move != curCard)
                    {
                        curCard += 1;
                        curCard %= iconsGameCard.size();
                    }
                    else {
                        playerChip.get(lastPlayer).setIdCard(curCard);
                        playerChip.get(lastPlayer).getCoordinates().setX(iconsGameCard.get(curCard).getCoordinates().getX());
                        playerChip.get(lastPlayer).getCoordinates().setY(iconsGameCard.get(curCard).getCoordinates().getY());
                        move = -1;
                    }

                limitX = Math.abs(iconsGameCard.get(curCard).getCoordinates().getX()
                - playerChip.get(lastPlayer).getCoordinates().getX());
                limitY = Math.abs(iconsGameCard.get(curCard).getCoordinates().getY()
                -  playerChip.get(lastPlayer).getCoordinates().getY());
            }
        }

        paintPlayers(g);
    }

    private void drawBuilding(Graphics g, IconGameCard card)
    {
        Font fontUpperCase = new Font("Impact", Font.ITALIC, 20);
        Font fontLowerCase = new Font(Font.DIALOG_INPUT, Font.BOLD, 15);

        int stepY = 15;
        g.drawImage(card.getIconBuilding().getImageOfBuilding(),
                card.getCoordinates().getX() + gamePanelSizes.getBuildingIndentX(),
                card.getCoordinates().getY() + gamePanelSizes.getBuildingIndentY(),
                gamePanelSizes.getCardBuildingWidth(), gamePanelSizes.getCardBuildingHeight(), null);

        g.setFont(fontUpperCase);
        g.drawString(card.getIconBuilding().getBuilding().getName(),
                card.getCoordinates().getX()+gamePanelSizes.getNameIndentX(),
                card.getCoordinates().getY()+gamePanelSizes.getNameIndentY());

        g.setFont(fontLowerCase);

        if (card.getCard().getBuilding().getBuildingType().equals(BuildingType.BUSINESS))
        {
            drawRecordBuilding(g, cardLabels.get(0),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getCostIndentY());
            drawRecordBuilding(g, String.valueOf(card.getIconBuilding().getBuilding().getCost()),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getCostIndentY()+stepY);

            drawRecordBuilding(g, cardLabels.get(1),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getTaxIndentY());
            drawRecordBuilding(g, String.valueOf(card.getIconBuilding().getBuilding().getTax()),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getTaxIndentY() + stepY);

            drawRecordBuilding(g, cardLabels.get(2),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getOwnerIndentY());
        }

        if (card.getIconBuilding().getBuilding().isHasOwner())
        {
            drawRecordBuilding(g, card.getIconBuilding().getBuilding().getOwner().getName(),
                    card.getCoordinates().getX() + gamePanelSizes.getRecordIndentX(),
                    card.getCoordinates().getY() + gamePanelSizes.getOwnerIndentY() + stepY);
        }
    }

    private void drawRecordBuilding(Graphics g, String record, int x, int y)
    {
        g.drawString(record, x, y);
    }

    private void createIconsCard(ArrayList<Card> cards)
    {
        int x = 0;
        int y = 0;
        int j = 0;

        byte[] image4 = imageRepository.getImage(4, TypeOfIcon.IconGameCard).getImage();
        byte[] image2 = imageRepository.getImage(2, TypeOfIcon.IconGameCard).getImage();
        byte[] image3 = imageRepository.getImage(3, TypeOfIcon.IconGameCard).getImage();
        for (int i = 0; i < 6; i++)
        {
            iconsGameCard.add(new IconGameCard(imageRepository.getImage(2, TypeOfIcon.IconGameCard).getImage()).setCard(cards.get(i), image4, image2, image3, imageRepository.getImage(cards.get(i).getIdCard(), TypeOfIcon.IconBuilding).getImage())
                    .setCoordinates(new Coordinates(x, y)));

            x += gamePanelSizes.getCardWidth();
        }

        for (int i = 6; i < 11; i++)
        {
            j++;
            iconsGameCard.add(i,
                    new IconGameCard(image2).setCard(cards.get(i), image4, image2, image3, imageRepository.getImage(cards.get(i).getIdCard(), TypeOfIcon.IconBuilding).getImage())
                            .setCoordinates(new Coordinates
                                    (gamePanelSizes.getCardWidth()*5, gamePanelSizes.getCardHeight()*j)));
        }

        x = gamePanelSizes.getCardWidth()*4;
        y = gamePanelSizes.getCardHeight()*5;

        for (int i = 11; i < 16; i++)
        {
            iconsGameCard.add(i,  new IconGameCard(image2).setCard(cards.get(i), image4, image2, image3,imageRepository.getImage(cards.get(i).getIdCard(), TypeOfIcon.IconBuilding).getImage())
                    .setCoordinates(new Coordinates(x, y)));

            x -= gamePanelSizes.getCardWidth();
        }

       j = 5;
        for (int i = 16; i < 20; i++)
        {
            j--;
            iconsGameCard.add(i,
                    new IconGameCard(image2).setCard(cards.get(i), image4, image2, image3, imageRepository.getImage(cards.get(i).getIdCard(), TypeOfIcon.IconBuilding).getImage())
                            .setCoordinates(new Coordinates(0, gamePanelSizes.getCardHeight()* j)));
        }
    }

    public void setMove(Player player, int move)
    {
        this.lastPlayer = player;
        this.move = move;
        limitX = -1;
        limitY = -1;
        moveCount = 0;
        curCard = playerChip.get(lastPlayer).getIdCard();
    }

    public int getMove()
    {
        return move;
    }
}
