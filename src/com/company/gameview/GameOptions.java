package com.company.gameview;

import com.company.controller.Controller;
import com.company.controller.ImageRepository;
import com.company.gameview.icons.TypeOfIcon;
import com.company.gameview.listeners.ShowSavedGamesListener;
import com.company.gameview.listeners.StartGameListener;
import javax.swing.*;
import java.awt.*;

public class GameOptions extends JFrame
{

    private Controller controller;
    private ImageRepository imageRepository;

    public GameOptions(Controller controller, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        this.controller = controller;
        configuration();
    }

    private void configuration()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setExtendedState(MAXIMIZED_BOTH);
        setResizable(false);

        PanelOption panelOption = new PanelOption();
        add(panelOption);
        setVisible(true);
        panelOption.repaint();
    }

    class PanelOption extends JPanel
    {
        private JButton startGame;
        private JButton continueGame;

        private Image backGround;

        private String nameStartGame = "START NEW GAME";
        private String nameContinueGame = "CONTINUE SAVED GAME";

        PanelOption()
        {
            backGround = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.BackgroudMenu).getImage()).getImage();
            setLayout(new GridBagLayout());

            ImageIcon icon = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.GrayButton).getImage());

            GridBagConstraints c  = GridConstraint.getConstraint();
            c.insets = new Insets(300, 20, 20, 20);
            startGame = new JButton(nameStartGame, icon);
            add(configureButton(startGame), c);
            startGame.addMouseListener(new StartGameListener(controller));

            c.insets = new Insets(0, 100, 0, 100);
            continueGame = new JButton(nameContinueGame, icon);
            add(configureButton(continueGame), c);
            continueGame.addMouseListener(new ShowSavedGamesListener(controller));
        }

        private JButton configureButton(JButton button)
        {
            button.setVerticalTextPosition(SwingConstants.CENTER);
            button.setHorizontalTextPosition(SwingConstants.CENTER);
            button.setFont(new Font("showcard gothic", Font.BOLD, 20));
            button.setOpaque(false);
            button.setBorder(null);
            button.setContentAreaFilled(false);

            button.setSize(20, 20);
            return button;
        }

        @Override
        protected void paintComponent(Graphics g)
        {
            g.drawImage(backGround, 0, 0, getSize().width, getSize().height, null);

            g.setFont(new Font("showcard gothic", Font.BOLD, 100));
            g.drawString("MONOPOLY",
                    (int) (this.getSize().getWidth()/3),
                    this.getHeight()/5);
        }
    }
}
