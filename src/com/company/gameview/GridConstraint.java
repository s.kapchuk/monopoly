package com.company.gameview;

import java.awt.*;

public class GridConstraint
{

    private GridConstraint()
    {
        //is only for private access
    }

    public static GridBagConstraints getConstraint()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = .25;
        c.weighty = .25;
        c.insets = new Insets(20, 20, 20, 20);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridheight = 1;
        c.fill = GridBagConstraints.BOTH;
        return c;
    }
}
