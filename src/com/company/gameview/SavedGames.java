package com.company.gameview;

import com.company.controller.Controller;
import com.company.controller.ImageRepository;
import com.company.gameview.icons.TypeOfIcon;
import com.company.gameview.listeners.StartSavedGameListener;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SavedGames extends JFrame
{
    private ImageRepository imageRepository;
    private JLabel headerTable = new JLabel("SAVED GAMES");
    private JTable tablePlayersScore;

    private JButton buttonLoadGame;
    private String loadGame = "LOAD GAME";

    private HashMap<String, ArrayList<String>> information;

    private ImageIcon icon;

    private String nameFont = "showcard gothic";
    Controller controller;

    public SavedGames(Map<String, ArrayList<String>> information, Controller controller, ImageRepository imageRepository)
    {
        this.imageRepository = imageRepository;
        this.information = (HashMap<String, ArrayList<String>>) information;

        icon = new ImageIcon(imageRepository.getImage(0, TypeOfIcon.GrayButton).getImage());

        this.controller = controller;

        configuration();

        JPanel panel = new JPanel();
        panel.setBackground(Color.lightGray);

        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = getConstraint();

        buttonLoadGame = new JButton(loadGame, icon);
        buttonLoadGame.setHorizontalAlignment(SwingConstants.LEFT);
        buttonLoadGame.addMouseListener(new StartSavedGameListener(controller, this));

        tablePlayersScore = new JTable(createTableModel((HashMap<String, ArrayList<String>>) information));
        addScrollPane(tablePlayersScore, c, headerTable, panel);
        panel.add(configureButton(buttonLoadGame), c);

        add(panel);
    }

    private void addScrollPane(JTable table, GridBagConstraints c, JLabel label, JPanel panel)
    {
        configureTable(table);
        JScrollPane pane = new JScrollPane(table);
        label.setFont(new Font(nameFont, Font.BOLD | Font.ITALIC, 25));

        c.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(label, c);
        panel.add(pane, c);
    }

    private void configuration()
    {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setSize(new Dimension(800, 800));
        setResizable(false);
    }

    private GridBagConstraints getConstraint()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = .25;
        c.weighty = .25;
        c.insets = new Insets(0, 20, 0, 20);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridheight = 1;
        c.fill = GridBagConstraints.BOTH;
        return c;
    }

    private void configureTable(JTable table)
    {

        table.setRowHeight(50);

        for (String nameOfColumn : information.keySet())
        {
            table.getColumn(nameOfColumn).setPreferredWidth(230);
            table.getColumn(nameOfColumn).setMinWidth(230);
            table.getColumn(nameOfColumn).setMaxWidth(230);

        }

        table.setBorder(new MatteBorder(2, 2, 2, 2, Color.green));
        table.setFont(new Font(nameFont, Font.BOLD, 25));
        table.setGridColor(Color.GREEN);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }

    private JButton configureButton(JButton button)
    {
        button.setVerticalTextPosition(SwingConstants.CENTER);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.setFont(new Font("showcard gothic", Font.BOLD, 15));
        button.setOpaque(false);
        button.setBorder(null);
        button.setContentAreaFilled(false);

        button.setSize(20, 20);
        return button;
    }

    private DefaultTableModel createTableModel(HashMap<String, ArrayList<String>> columnsData)
    {
        DefaultTableModel model = new DefaultTableModel();

        for (Map.Entry<String, ArrayList<String>> nameOfColumn : columnsData.entrySet())
        {
            model.addColumn(nameOfColumn.getKey(), columnsData.get(nameOfColumn.getKey()).toArray());
        }

        return model;
    }

    public String getSelectedName()
    {
       return information.get("Player").get(tablePlayersScore.getSelectedRow());
    }
}
