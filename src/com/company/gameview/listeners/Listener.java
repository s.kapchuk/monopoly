package com.company.gameview.listeners;

import com.company.controller.Controller;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Listener implements MouseListener
{
    protected Controller controller;

    public Listener(Controller controller)
    {
        this.controller = controller;
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        //want to have only one answer to mouse action
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        //want to have only one answer to mouse action
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        //want to have only one answer to mouse action
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
        //want to have only one answer to mouse action
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
        //want to have only one answer to mouse action
    }
}
