package com.company.gameview.listeners;

import com.company.controller.Controller;
import javax.swing.*;
import java.awt.event.MouseEvent;

public class StartGameListener extends Listener
{


    public StartGameListener(Controller controller)
    {
        super(controller);
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        Thread thread = new Thread(() -> {
            String name = "";
            while (name.equals(""))
             name = JOptionPane.showInputDialog(" Введите имя игрока ");

            controller.game(name);
        });
        thread.start();
    }
}
