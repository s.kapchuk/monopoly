package com.company.gameview.listeners;

import com.company.controller.Controller;
import java.awt.event.MouseEvent;

public class ShowSavedGamesListener extends Listener
{
    public ShowSavedGamesListener(Controller controller)
    {
        super(controller);
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        Thread thread = new Thread(this::run);
        thread.start();
    }

    private void run()
    {
        controller.showSavedGames();
    }
}
