package com.company.gameview.listeners;

import com.company.controller.Controller;
import javax.swing.*;
import java.awt.event.MouseEvent;

public class ShowMenuListener extends Listener
{
    public ShowMenuListener(Controller controller)
    {
        super(controller);
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        int answer = JOptionPane.showConfirmDialog(null, "Хотите сохранить игру перед выходом?");
        Thread thread = new Thread(() -> {
            if (answer == JOptionPane.YES_OPTION)
                controller.saveGameAndShowMenu();
            else
                controller.showMenu();
        });
        thread.start();
    }
}
