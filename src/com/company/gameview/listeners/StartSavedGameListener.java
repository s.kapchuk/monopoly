package com.company.gameview.listeners;

import com.company.controller.Controller;
import com.company.gameview.SavedGames;
import java.awt.event.MouseEvent;

public class StartSavedGameListener extends Listener
{
    private SavedGames frame;

    public StartSavedGameListener(Controller controller, SavedGames frame)
    {
        super(controller);
        this.frame = frame;
        //зная имя игрока загружаем данные
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        Thread thread = new Thread(() -> {
        String name = frame.getSelectedName();
            controller.continueGame(controller.loadStateOfGame(name));
        });
        thread.start();
        //метод для выбора имени игрока, чтобы знать, какие данные загружать

        /* создать метод контроллера для загрузки данных либо загружаем их в контроллере */
    }
}
