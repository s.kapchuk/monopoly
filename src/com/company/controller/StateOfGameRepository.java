package com.company.controller;

import com.company.model.StateOfGameEntity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class StateOfGameRepository
{
    private EntityManagerFactory entityManagerFactory;

    public StateOfGameRepository(EntityManagerFactory entityManagerFactory)
    {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Card findCardByPosition(int idCard, long id_stateOfGameEntity)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager.createQuery("select p from Card p where p.idCard = :idCard and p.id_stateOfGameEntity = :id_stateOfGameEntity", Card.class)
                .setParameter("idCard", idCard)
                .setParameter("id_stateOfGameEntity", id_stateOfGameEntity)
                .getSingleResult();
    }

    public List<Player> findPlayersForSavedGames()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager.createQuery("select p from Player p", Player.class)
                .getResultList();
    }

    @Transactional
    public StateOfGameEntity findSaveByPlayerName(String name)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Player player = entityManager.createQuery("select p from Player p where p.name = :name", Player.class)
                .setParameter("name", name)
                .getSingleResult();
        long stateOfGameId = player.getId_stateOfGameEntity();
        StateOfGameEntity stateOfGameEntity = entityManager.createQuery("select p from StateOfGameEntity p where p.id = :stateOfGameId", StateOfGameEntity.class)
                .setParameter("stateOfGameId", stateOfGameId)
                .getSingleResult();
        stateOfGameEntity = entityManager.merge(stateOfGameEntity);
        entityManager.getTransaction().commit();

        return stateOfGameEntity;
    }

    public List<Building> findSavedBuildings(List<Card> cards)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        List<Building> buildings = new ArrayList<>();

        for (Card c: cards) {
            c.setBuilding(entityManager.createQuery("select b from Building b where b.id = :id", Building.class)
                    .setParameter("id", c.getBuilding_id())
                    .getSingleResult());
        }
        entityManager.getTransaction().commit();
        return buildings;
    }

    public StateOfGameEntity saveGame()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        StateOfGameEntity stateOfGameEntity = new StateOfGameEntity();

        entityManager.getTransaction().begin();
        stateOfGameEntity = entityManager.merge(stateOfGameEntity);
        entityManager.getTransaction().commit();
        return stateOfGameEntity;
    }

    public StateOfGameEntity savePlayers(List<Player> players)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        StateOfGameEntity stateOfGameEntity = entityManager.createQuery("select p from StateOfGameEntity p where p.id = :id", StateOfGameEntity.class)
                .setParameter("id", players.get(0).getId_stateOfGameEntity())
                .getSingleResult();

        for (Player p : players) {
            p.setStateOfGameEntity(stateOfGameEntity);
            stateOfGameEntity.addPlayer(p);
            if (p.hasId()) {
                entityManager.merge(p);
            }
            else {
                entityManager.persist(p);
            }
        }
        stateOfGameEntity = entityManager.merge(stateOfGameEntity);
        entityManager.getTransaction().commit();

        return stateOfGameEntity;
    }

    public StateOfGameEntity saveCards(List<Card> cards, List<Building> buildings)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        entityManager.getTransaction().begin();
        for (Building b : buildings) {
            if (b.hasId()) {
                entityManager.merge(b);
            }
            else {
                entityManager.persist(b);
            }
        }
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        StateOfGameEntity stateOfGameEntity = entityManager.createQuery("select c from StateOfGameEntity c where c.id = :id", StateOfGameEntity.class)
                .setParameter("id", cards.get(0).getId_stateOfGameEntity())
                .getSingleResult();

        int i = 0;
        for (Card c : cards) {
            c.setBuilding_id(buildings.get(i).getId());
            i++;
            c.setStateOfGameEntity(stateOfGameEntity);
            stateOfGameEntity.addCard(c);
            entityManager.merge(c);
        }
        stateOfGameEntity = entityManager.merge(stateOfGameEntity);
        entityManager.getTransaction().commit();
        return stateOfGameEntity;
    }

    public StateOfGameEntity savePositionOfPlayers(List<PlayerCard> playerCard)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        StateOfGameEntity stateOfGameEntity = entityManager.createQuery("select pc from StateOfGameEntity pc where pc.id = :id", StateOfGameEntity.class)
                .setParameter("id", playerCard.get(0).getId_stateOfGameEntity())
                .getSingleResult();

        for (PlayerCard p : playerCard) {
            p.setStateOfGameEntity(stateOfGameEntity);
            stateOfGameEntity.addPlayerCards(p);
            entityManager.persist(p);
        }
        stateOfGameEntity = entityManager.merge(stateOfGameEntity);
        entityManager.getTransaction().commit();

        return stateOfGameEntity;
    }
}
