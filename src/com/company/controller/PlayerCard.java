package com.company.controller;

import com.company.model.StateOfGameEntity;
import javax.persistence.*;

@Entity
public class PlayerCard
{
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long Id;

    @Column
    private long playerId;

    @Column
    private long cardId;

    @Column
    private long id_stateOfGameEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_stateOfGameEntity", updatable = false, nullable = true, insertable = false)
    private StateOfGameEntity stateOfGameEntity;

    public PlayerCard setId_stateOfGameEntity(long id_stateOfGameEntity)
    {
        this.id_stateOfGameEntity = id_stateOfGameEntity;
        return this;
    }

    public long getId_stateOfGameEntity() {
        return id_stateOfGameEntity;
    }

    public long getCardId() {
        return cardId;
    }

    public PlayerCard setCardId(long cardId)
    {
        this.cardId = cardId;
        return this;
    }

    public PlayerCard setPlayerId(long playerId)
    {
        this.playerId = playerId;
        return this;
    }

    public long getPlayerId()
    {
        return playerId;
    }

    public void setStateOfGameEntity(StateOfGameEntity stateOfGameEntity)
    {
        this.stateOfGameEntity = stateOfGameEntity;
        id_stateOfGameEntity = stateOfGameEntity.getId();
    }
}
