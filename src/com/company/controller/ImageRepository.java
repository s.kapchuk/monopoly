package com.company.controller;

import com.company.gameview.icons.TypeOfIcon;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ImageRepository
{
    private EntityManagerFactory entityManagerFactory;
    private final int NUMBER_OF_CARDS = 20;
    private final int NUMBER_OF_CHIPS = 4;
    private final int NUMBER_OF_GAMECARD = 3;
    private final int NUMBER_OF_FIRST_GAMECARD = 2;

    private final String PNG = "png";
    private final String JPG = "jpg";
    private final String GIF = "gif";

    public ImageRepository(EntityManagerFactory entityManagerFactory)
    {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void saveImage(Integer id, String source, int shiftId, String fileFormat)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        File file;
        if (id != null)
            file = new File("res\\" + source + id + "." + fileFormat);
        else {
            file = new File("res\\" + source + "." + fileFormat);
            id = 0;
        }

        byte[] bytedFile = new byte[(int) file.length()];

        try (FileInputStream fileInputStream = new FileInputStream(file))
        {
            int n = 0;
            do {
                n = fileInputStream.read(bytedFile);
            }
            while (n > 0);
        }
        catch (IOException e)
        {
            e.getCause();
        }

        Image image = new Image();
        image.setId(id + shiftId);
        image.setImage(bytedFile);
        entityManager.persist(image);
        entityManager.getTransaction().commit();
    }

    public void initImages() {
        for (int i = 0; i < NUMBER_OF_CARDS; i++)
        {
            saveImage(i, "", 0, JPG);
        }

        for (int i = 0; i < NUMBER_OF_CHIPS; i++)
        {
            saveImage(i, "hat", NUMBER_OF_CARDS, PNG);
        }

        for (int i = NUMBER_OF_FIRST_GAMECARD; i < NUMBER_OF_GAMECARD + NUMBER_OF_FIRST_GAMECARD; i++)
        {
            saveImage(i, "card", NUMBER_OF_CARDS + NUMBER_OF_CHIPS - NUMBER_OF_FIRST_GAMECARD, PNG);
        }

        saveImage(null, "man", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD, PNG);
        saveImage(null, "money", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 1, JPG);
        saveImage(null, "grayButton", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 2, PNG);
        saveImage(null, "dice (2)", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 3, GIF);
        saveImage(null, "buttonMakeTurn", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 4, PNG);
        saveImage(null, "background", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 5, JPG);
        saveImage(null, "backgroudMenu", NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 6, PNG);

    }

    public Image getImage(int id, TypeOfIcon typeOfIcon)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        if (typeOfIcon == TypeOfIcon.IconChip)
        {
            id = id + NUMBER_OF_CARDS;
        }
        else
            if (typeOfIcon == typeOfIcon.IconGameCard)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS - NUMBER_OF_FIRST_GAMECARD;
        }
        else if (typeOfIcon == typeOfIcon.IconPlayer)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD;
        }
        else if (typeOfIcon == typeOfIcon.Money) {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 1;
        }
        else if (typeOfIcon == typeOfIcon.GrayButton)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 2;
        }
        else if (typeOfIcon == typeOfIcon.Dice)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 3;
        }
        else if (typeOfIcon == typeOfIcon.ButtonMakeTurn)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 4;
        }
        else if (typeOfIcon == typeOfIcon.Background)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 5;
        }
        else if (typeOfIcon == typeOfIcon.BackgroudMenu)
        {
            id = id + NUMBER_OF_CARDS + NUMBER_OF_CHIPS + NUMBER_OF_GAMECARD + 6;
        }

        return entityManager.createQuery("select p from Image p where p.id = :id", Image.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public boolean isHasImages() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager.createQuery("select p from Player p", Player.class)
                .getResultList() != null;
    }
}
