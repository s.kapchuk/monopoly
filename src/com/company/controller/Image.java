package com.company.controller;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Image
{

    @javax.persistence.Id
    private int Id;

    @Column
    private byte[] image;

    public Image setId(int id)
    {
        Id = id;
        return this;
    }

    public Image setImage(byte[] image)
    {
        this.image = image;
        return this;
    }

    public int getId()
    {
        return Id;
    }

    public byte[] getImage()
    {
        return image;
    }
}
