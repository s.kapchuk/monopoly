package com.company.controller;

import com.company.model.StateOfGameEntity;

import javax.persistence.*;

@Entity
public class Card
{
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private int idCard;

    @Column
    private long building_id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "building_id", referencedColumnName = "Id", updatable = false, nullable = true, insertable = false)
    private Building building;

    @Column
    private long id_stateOfGameEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_stateOfGameEntity", updatable = false, nullable = true, insertable = false)
    private StateOfGameEntity stateOfGameEntity;


    public long getId() {
        return id;
    }

    public void setBuilding_id(long building_id) {
        this.building_id = building_id;
    }

    public long getBuilding_id() {
        return building_id;
    }

    public void setId_stateOfGameEntity(long id_stateOfGameEntity) {
        this.id_stateOfGameEntity = id_stateOfGameEntity;
    }

    public Card setBuilding(Building building)
    {
        this.building = building;
        return this;
    }

    public void setStateOfGameEntity(StateOfGameEntity stateOfGameEntity)
    {
        this.stateOfGameEntity = stateOfGameEntity;
        id_stateOfGameEntity = stateOfGameEntity.getId();
    }

    public long getId_stateOfGameEntity()
    {
        return id_stateOfGameEntity;
    }

    public Card setIdCard(int idCard)
    {
        if (idCard < 0)
            throw new IllegalArgumentException("Id of card should be greater then 0");
        this.idCard = idCard;
        return this;
    }

    public Building getBuilding()
    {
        return building;
    }

    public int getIdCard()
    {
        return idCard;
    }

    public boolean isHasBuilding()
    {
        return building != null;
    }
}
