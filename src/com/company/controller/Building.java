package com.company.controller;

import javax.persistence.*;

@Entity
public class Building
{
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_id")
    private Player owner;

    @Column
    private int cost;

    @Column
    private int tax;

    @Column
    private String name;

    @Column
    private BuildingType buildingType;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "building")
    private Card card;


    public long getId()
    {
        return id;
    }

    public Building setCost(int cost)
    {
        this.cost = cost;
        if (cost < 0)
            throw new IllegalArgumentException("Cost cant be less, then 0");
        return this;
    }

    public Building setTax(int tax)
    {
        this.tax = tax;
        if (tax < 0)
            throw new IllegalArgumentException("Tax cant be less, then 0");
        return this;
    }

    public Building setName(String name)
    {
        this.name = name;
        return this;
    }

    public Building setOwner(Player owner)
    {
        this.owner = owner;
        return this;
    }

    public int getCost()
    {
        return cost;
    }

    public int getTax()
    {
        return tax;
    }

    public Player getOwner()
    {
        return owner;
    }

    public boolean isHasOwner()
    {
        return owner != null;
    }

    public String getName()
    {
        return name;
    }

    public Building setBuildingType(BuildingType buildingType)
    {
        this.buildingType = buildingType;
        return this;
    }

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public boolean hasId()
    {
        return id != null;
    }
}

