package com.company.controller;

import com.company.model.StateOfGameEntity;
import javax.persistence.*;

@Entity
public class Player implements Comparable
{
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column
    private String name;

    @Column
    private int money;

    @Column
    private boolean isRealPlayer;

    @Column
    private int shiftX;

    @Column
    private long id_stateOfGameEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_stateOfGameEntity", updatable = false, nullable = true, insertable = false)
    private StateOfGameEntity stateOfGameEntity;

    public void setId_stateOfGameEntity(long id_stateOfGameEntity) {
        this.id_stateOfGameEntity = id_stateOfGameEntity;
    }

    public void setStateOfGameEntity(StateOfGameEntity stateOfGameEntity)
    {
        this.stateOfGameEntity = stateOfGameEntity;
        id_stateOfGameEntity = stateOfGameEntity.getId();
    }

    public int getShiftX()
    {
        return shiftX;
    }

    public Player setShiftX(int shiftX)
    {
        this.shiftX = shiftX;
        return this;
    }

    public long getId_stateOfGameEntity()
    {
        return id_stateOfGameEntity;
    }

    public String getName()
    {
        return name;
    }

    public int getMoney()
    {
        return  money;
    }

    public boolean getIsRealPlayer()
    {
        return isRealPlayer;
    }

    public Player setMoney(int money)
    {
        if (money < 0)
            throw new IllegalArgumentException("Money of player should be greater then 0");

        this.money = money;
        return this;
    }

    public Long getId()
    {
        return Id;
    }

    public Player setName(String name)
    {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Player must have name");

        this.name = name;
        return this;
    }

    public Player setIsRealPlayer(boolean isRealPlayer)
    {
        this.isRealPlayer = isRealPlayer;
        return this;
    }

    public boolean hasId()
    {
        return Id != null;
    }

    @Override
    public int compareTo(Object o)
    {
        Player p = (Player) o;
        return Integer.compare(this.getShiftX(), p.getShiftX());
    }
}
