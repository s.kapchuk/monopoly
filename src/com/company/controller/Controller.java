package com.company.controller;

import com.company.FrameInitializer;
import com.company.gameview.GameField;
import com.company.gameview.GameOptions;
import com.company.gameview.SavedGames;
import com.company.model.Model;
import com.company.model.StateOfGame;
import com.company.model.StateOfGameEntity;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.*;

public class Controller {
    private GameField gameField;
    private Model model;
    private List<Player> players;
    private FrameInitializer initializer;
    private GameOptions gameOptions;
    private StateOfGameRepository stateOfGameRepository;
    private ImageRepository imageRepository;

    public Controller(EntityManagerFactory entityManagerFactory)
    {
        stateOfGameRepository = new StateOfGameRepository(entityManagerFactory);
        imageRepository = new ImageRepository(entityManagerFactory);

        initializer = new FrameInitializer();

        model = new Model(initializer.getPlayers(), initializer.getCards());
        players = initializer.getPlayers();

        gameOptions = new GameOptions(this, imageRepository);
    }

    public void game(String nameOfPlayer)
    {
        gameOptions.setVisible(false);

        players.get(0).setName(nameOfPlayer);
        gameField = new GameField(initializer.getPlayers(), initializer.getCards(), this, imageRepository);

        StateOfGame stateOfGame;

        while ((stateOfGame = model.isGameContinue()).getIsGameContinue())
        {
            for (Player player : players)
            {
                if (player.getMoney() > 0)
                {
                    stateOfGame = turnOfPlayer(player, stateOfGame);
                }
            }
        }
    }

    private StateOfGame turnOfPlayer(Player player, StateOfGame stateOfGame)
    {
        int numberOnDice = model.throwDice();
        if (player.getIsRealPlayer())
        {
            gameField.showMakeTurn(numberOnDice);
        }

        int newPosition = model.newPositionOFPlayer(player, numberOnDice);
        gameField.repaint();
        gameField.showMoveOfPlayer(player, newPosition);

        if (model.shouldMakeBuyingChoose(player))
        {
            stateOfGame = makeBuyingChoose(player, stateOfGame);
        }

        if (model.shouldMakeLotteryChoose(player))
        {
            int result =  model.throwDice();

            if (player.getIsRealPlayer())
            {
                stateOfGame.setAnswerOnChoose(gameField.showMakeLotteryChoose());
                if (stateOfGame.getAnswerOnChoose())
                    gameField.showMakeTurn(result);
            }
            else
                 stateOfGame = model.tryLottery(player, result);
        }
        return stateOfGame;
    }

    private StateOfGame makeBuyingChoose(Player player, StateOfGame stateOfGame)
    {
        if (player.getIsRealPlayer())
        {
            stateOfGame.setAnswerOnChoose(gameField.showMakeBuyBuildingChoose());
        }
        else {
            stateOfGame.setAnswerOnChooseForBot(player);
        }

        gameField.repaint();
        return model.buyBuilding(player, stateOfGame.getAnswerOnChoose());
    }

    public void saveGameAndShowMenu()
    {
        StateOfGameEntity stateOfGameEntity = stateOfGameRepository.saveGame();

        ArrayList<Player> players = new ArrayList<>(model.getStateOfGame().getPositionsOfPlayers().keySet());
        ArrayList<Card> playersPositions = new ArrayList<>(model.getStateOfGame().getPositionsOfPlayers().values());
        ArrayList<Card> cards = new ArrayList<>(model.getStateOfGame().getPositionOfCards().values());
        ArrayList<Building> buildings = new ArrayList<>();

        for (Player p : players)
        {
            p.setId_stateOfGameEntity(stateOfGameEntity.getId());
        }

        for (Card c : cards)
        {
            c.setId_stateOfGameEntity(stateOfGameEntity.getId());
            buildings.add(c.getBuilding());
        }

        stateOfGameRepository.savePlayers(players);
        stateOfGameEntity = stateOfGameRepository.saveCards(cards, buildings);

        ArrayList<PlayerCard> playerCards = new ArrayList<>();
        Card card;

        for (int i = 0; i < 4; i++)
        {
            card = stateOfGameRepository.findCardByPosition(playersPositions.get(i).getIdCard(), stateOfGameEntity.getId());
            playerCards.add(new PlayerCard().setPlayerId(stateOfGameEntity.getPlayers().get(i).getId()).setCardId(card.getIdCard()).setId_stateOfGameEntity(stateOfGameEntity.getId()));
        }

        stateOfGameRepository.savePositionOfPlayers(playerCards);
        showMenu();
    }

    public void showMenu()
    {
        gameOptions.setVisible(true);
        gameField.dispose();
        gameField = null;
    }

    public void showSavedGames()
    {
        HashMap<String, ArrayList<String>> information = new HashMap<>();

        ArrayList<String> listPlayers = new ArrayList<>();
        ArrayList<String> playersScore = new ArrayList<>();

        List<Player> players = stateOfGameRepository.findPlayersForSavedGames();

        for (Player p : players)
        {
            String name = p.getName();
            if (p.getIsRealPlayer())
            {
                listPlayers.add(name);
                playersScore.add(String.valueOf(p.getMoney()));
            }
        }

        information.put("Player",  listPlayers);

        information.put("Score", playersScore);

        SavedGames savedGames = new SavedGames(information, this, imageRepository);
        savedGames.setVisible(true);
    }

    public StateOfGameEntity loadStateOfGame(String name)
    {
        StateOfGameEntity stateOfGameEntity = stateOfGameRepository.findSaveByPlayerName(name);
        stateOfGameRepository.findSavedBuildings(stateOfGameEntity.getCards());
        return stateOfGameEntity;
    }

    public void continueGame(StateOfGameEntity stateOfGameEntity)
    {
        List<Player> players = stateOfGameEntity.getPlayers();
        List<Card> cards = stateOfGameEntity.getCards();
        List<Card> playerPosition = new ArrayList<>();

        for (PlayerCard playerCard : stateOfGameEntity.getPlayerCards())
        {
            playerPosition.add(stateOfGameRepository.findCardByPosition((int)playerCard.getCardId(), stateOfGameEntity.getId()));
        }

        gameOptions.setVisible(false);
        StateOfGame stateOfGame = new StateOfGame(players, cards, playerPosition);

        gameField = new GameField(players, cards, this, imageRepository);
        int i = 0;
        for (Player player : players)
        {
            gameField.showMoveOfPlayer(player, playerPosition.get(i).getIdCard());
            i++;
        }

        Collections.sort(players);
        model.setStateOfGame(stateOfGame);
        while ((stateOfGame = model.isGameContinue()).getIsGameContinue())
        {
            for (Player player : players)
            {
                if (player.getMoney() > 0)
                {
                    stateOfGame = turnOfPlayer(player, stateOfGame);
                }
            }
        }
    }
}
