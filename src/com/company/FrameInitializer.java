package com.company;

import com.company.controller.Building;
import com.company.controller.BuildingType;
import com.company.controller.Card;
import com.company.controller.Player;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class FrameInitializer
{
    private ArrayList<Player> players;
    private ArrayDeque<Building> buildings;
    private ArrayList<Card> cards;

    public FrameInitializer()
    {
        createPlayers();
        initializeBuildings();
        createCards();
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public List<Card> getCards()
    {
        return cards;
    }

    private void createPlayers()
    {

        players = new ArrayList<>();
        players.add(new Player().setName("Bob").setMoney(10000).setShiftX(0).setIsRealPlayer(true));
        players.add(new Player().setName("Alisa").setMoney(10000).setShiftX(10).setIsRealPlayer(false));
        players.add(new Player().setName("Jack").setMoney(10000).setShiftX(20).setIsRealPlayer(false));
        players.add(new Player().setName("Mikki").setMoney(10000).setShiftX(30).setIsRealPlayer(false));

    }

    private void createCards()
    {
        cards = new ArrayList<>();

        for (int i = 0; i < 20; i++)
        {
            cards.add(i, new Card().setIdCard(i));
            cards.get(i).setBuilding(buildings.pop());
        }
    }

    private ArrayDeque<Building> initializeBuildings()
    {
        buildings = new ArrayDeque<>();

        String nameOfBuilding = "Building";
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 500, 3000, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 500, 3000, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),   nameOfBuilding, 400, 2200, BuildingType.BUSINESS));
        buildings.push(constructSpecialMonopolyObject(new Building(),  "Plus", BuildingType.PLUS));

        buildings.push(constructSpecialMonopolyObject(new Building(),  "Police",  BuildingType.POLICE));
        buildings.push(constructSpecialMonopolyObject(new Building(),  "Minus", BuildingType.MINUS));
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 400, 2200, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),   nameOfBuilding, 500, 2050, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),   nameOfBuilding, 400, 2200, BuildingType.BUSINESS));

        buildings.push(constructSpecialMonopolyObject(new Building(),  "Lottery", BuildingType.LOTTERY));
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 300, 1500, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 300, 1500, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(),  nameOfBuilding, 300, 1500, BuildingType.BUSINESS));
        buildings.push(constructSpecialMonopolyObject(new Building(),  "Plus", BuildingType.PLUS));

        buildings.push(constructSpecialMonopolyObject(new Building(), "Prison", BuildingType.PRISON));
        buildings.push(constructSpecialMonopolyObject(new Building(), "Minus",  BuildingType.MINUS));
        buildings.push(constructBuilding(new Building(), nameOfBuilding, 300, 1500, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(), nameOfBuilding, 200, 1000, BuildingType.BUSINESS));
        buildings.push(constructBuilding(new Building(), nameOfBuilding, 200, 1000, BuildingType.BUSINESS));
        buildings.push(constructSpecialMonopolyObject(new Building(),  "Start", BuildingType.START));

        return buildings;
    }

    private Building constructSpecialMonopolyObject(Building building, String nameBuilding, BuildingType buildingType)
    {
        return building.setName(nameBuilding).setBuildingType(buildingType);
    }

    private Building constructBuilding(Building building, String nameBuilding, int tax, int cost, BuildingType buildingType)
    {
        return building.setName(nameBuilding).setTax(tax).setCost(cost).setBuildingType(buildingType);
    }


}
