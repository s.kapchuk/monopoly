package com.company;

import com.company.controller.Controller;
import com.company.controller.ImageRepository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main
{
    public static void main(String[] args)
    {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("model");
        ImageRepository imageRepository = new ImageRepository(entityManagerFactory);
        if (!imageRepository.isHasImages())
            imageRepository.initImages();

        new Controller(entityManagerFactory);
    }
}
