package test.test;

import com.company.controller.PlayerCard;
import com.company.model.StateOfGameEntity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlayerCardTest
{
    private PlayerCard playerCard;
    private StateOfGameEntity stateOfGameEntity;

    @Before
    public void init()
    {
        playerCard = new PlayerCard();
        stateOfGameEntity = Mockito.mock(StateOfGameEntity.class);
        Mockito.when(stateOfGameEntity.getId()).thenReturn((long)1);
    }

    @Test
    public void setId_stateOfGameEntity()
    {
        playerCard.setId_stateOfGameEntity(2);
        assertThat(playerCard.getId_stateOfGameEntity(), equalTo((long)2));
    }

    @Test
    public void setStateOfGameEntity()
    {
        playerCard.setStateOfGameEntity(stateOfGameEntity);
        assertThat(playerCard.getId_stateOfGameEntity(), equalTo((long)1));
    }

    @Test
    public void setCardId()
    {
        playerCard.setCardId(1);
        assertThat(playerCard.getCardId(), equalTo((long)1));
    }

    @Test
    public void setPlayerId()
    {
        playerCard.setPlayerId(1);
        assertThat(playerCard.getPlayerId(), equalTo((long)1));
    }

}