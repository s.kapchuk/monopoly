package test.test;

import com.company.FrameInitializer;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class FrameInitializerTest
{
    private FrameInitializer frameInitializer;

    @Before
    public void init()
    {
        frameInitializer = new FrameInitializer();
    }

    @Test
    public void getPlayers()
    {
        assertThat(frameInitializer.getPlayers(),  notNullValue());
    }

    @Test
    public void getCards()
    {
        assertThat(frameInitializer.getCards(),  notNullValue());
    }

}