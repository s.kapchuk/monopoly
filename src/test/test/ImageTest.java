package test.test;

import com.company.controller.Image;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ImageTest
{

    private Image image = new Image();

    @Test
    public void setId()
    {
        image.setId(1);
        assertThat(image.getId(), equalTo(1));
    }

    @Test
    public void setImage()
    {
        byte[] array = new byte[]{0, 0, 1};
        image.setImage(array);
        assertThat(array, equalTo(image.getImage()));
    }

    @Test
    public void getId()
    {
        assertThat(image.getId(), notNullValue());
    }

}