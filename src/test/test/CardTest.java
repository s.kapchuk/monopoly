package test.test;

import com.company.controller.Building;
import com.company.controller.Card;

import com.company.model.StateOfGameEntity;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CardTest
{

    private Card card = new Card();

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setIdCard()
    {
        int idCard = -1;
        card.setIdCard(idCard);
    }

    @org.junit.Test
    public void getBuilding()
    {
        Building building = new Building();
        card.setBuilding(building);
        assertThat(building, equalTo(card.getBuilding()));
    }

    @org.junit.Test
    public void getIdCard()
    {
        int idCard = 2;
        card.setIdCard(idCard);
        assertThat(idCard, equalTo(card.getIdCard()));
    }

    @org.junit.Test
    public void isHasBuildingTrue()
    {
        Building building = new Building();
        card.setBuilding(building);
        assertThat(true, equalTo(card.isHasBuilding()));
    }

    @org.junit.Test
    public void isHasBuildingFalse()
    {
        card.setBuilding(null);
        assertThat(false, equalTo(card.isHasBuilding()));
    }

    @Test
    public void getId()
    {
        assertThat(card.getId(), greaterThanOrEqualTo((long)0));
    }

    @Test
    public void setStateEntity()
    {
        StateOfGameEntity entity = Mockito.mock(StateOfGameEntity.class);
        Mockito.when(entity.getId()).thenReturn((long)1);
        card.setStateOfGameEntity(entity);
        assertThat(card.getId_stateOfGameEntity(), equalTo(entity.getId()));
    }

    @Test
    public void setIdEntity()
    {
        card.setId_stateOfGameEntity(1);
        assertThat(card.getId_stateOfGameEntity(), equalTo((long)1));
    }

    @Test
    public void setBuildingId()
    {
        card.setBuilding_id(1);
        assertThat(card.getBuilding_id(), equalTo((long)1));
    }
}