package test.test.model;

import com.company.controller.Card;
import com.company.controller.Player;
import com.company.controller.PlayerCard;
import com.company.model.StateOfGameEntity;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.Assert.*;

public class StateOfGameEntityTest
{

    private StateOfGameEntity entity;

    @Before
    public void init()
    {
        entity = new StateOfGameEntity();
    }

    @Test
    public void setCards()
    {
        Card card = new Card();
        entity.addCard(card);
        assertSame(entity.getCards().get(0), card);
    }

    @Test
    public void setPlayers()
    {
        Player player = new Player();
        entity.addPlayer(player);
        assertSame(entity.getPlayers().get(0), player);
    }

    @Test
    public void addPlayerCards()
    {
        PlayerCard playerCard = new PlayerCard();
        entity.addPlayerCards(playerCard);
        assertThat(entity.getPlayerCards().get(0), equalTo(playerCard));
    }
}