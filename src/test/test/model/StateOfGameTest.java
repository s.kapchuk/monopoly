package test.test.model;

import com.company.FrameInitializer;
import com.company.controller.Card;
import com.company.controller.Player;
import com.company.model.Model;
import com.company.model.StateOfGame;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class StateOfGameTest
{
    private Model model;
    private List<Player> playerList;
    private List<Card> cards;
    private StateOfGame stateOfGame;

    @Before
    public void init()
    {
        FrameInitializer frameInitializer = new FrameInitializer();
        playerList = frameInitializer.getPlayers();
        cards = frameInitializer.getCards();

        model = new Model(playerList, cards);
    }

    @Test
    public void getPositionOfCards()
    {
        stateOfGame = model.isGameContinue();
        assertThat(stateOfGame.getPositionOfCards(), notNullValue());
    }

    @Test
    public void getPositionsOfPlayers()
    {
        stateOfGame = model.isGameContinue();
        assertThat(stateOfGame.getPositionsOfPlayers(), notNullValue());
    }

    @Test
    public void setIsGameContinue()
    {
        stateOfGame = model.isGameContinue();
        stateOfGame.setIsGameContinue(false);

        assertThat(stateOfGame.getIsGameContinue(), equalTo(false));
    }

    @Test
    public void setAnswerOnChoose()
    {
        stateOfGame = model.isGameContinue();
        stateOfGame.setAnswerOnChoose(0);

        assertThat(stateOfGame.getAnswerOnChoose(), equalTo(true));
    }

    @Test
    public void setAnswerOnChooseForBot()
    {
        stateOfGame = model.isGameContinue();
        stateOfGame.setAnswerOnChooseForBot(playerList.get(1));

        assertThat(stateOfGame.getAnswerOnChoose(), equalTo(true));
    }

    @Test
    public void createStateOfGame()
    {
        stateOfGame = new StateOfGame(playerList, cards);
        assertThat(stateOfGame.getPositionOfCards(), notNullValue());
        assertThat(stateOfGame.getPositionOfCards(), notNullValue());
    }

    @Test
    public void setAnswerForBotTrue()
    {
        stateOfGame = model.isGameContinue();
        model.newPositionOFPlayer(playerList.get(1), 3);
        stateOfGame.setAnswerOnChooseForBot(playerList.get(1));
        assertThat(stateOfGame.getAnswerOnChoose(), equalTo(true));
    }

    @Test
    public void setAnswerForBotFalse()
    {
        stateOfGame = model.isGameContinue();
        playerList.get(1).setMoney(100);
        model.newPositionOFPlayer(playerList.get(1), 3);
        stateOfGame.setAnswerOnChooseForBot(playerList.get(1));
        assertThat(stateOfGame.getAnswerOnChoose(), equalTo(false));
    }

    @Test
    public void testSavedPositions()
    {
        List<Card> playerPosition = Mockito.mock(List.class);
        Card card = Mockito.mock(Card.class);
        Mockito.when(playerPosition.get(Mockito.anyInt())).thenReturn(card);

        StateOfGame stateOfGame = new StateOfGame(playerList, cards, playerPosition);
        assertThat(card, equalTo(stateOfGame.getPositionsOfPlayers().get(playerList.get(2))));
    }
}