package test.test.model;

import com.company.FrameInitializer;
import com.company.controller.Card;
import com.company.controller.Player;
import com.company.model.Model;
import com.company.model.StateOfGame;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ModelTest
{
    private Model model;
    private List<Player> playerList;
    private List<Card> cards;

    @Before
    public void init()
    {
        FrameInitializer frameInitializer = new FrameInitializer();
        playerList = frameInitializer.getPlayers();
        cards = frameInitializer.getCards();

        model = new Model(playerList, cards);
    }

    @Test
    public void shouldMakeBuyingChoose()
    {
        int positionOfBuilding = 3;

        model.newPositionOFPlayer(playerList.get(0), positionOfBuilding);

        assertThat(model.shouldMakeBuyingChoose(playerList.get(0)), equalTo(true));
    }

    @Test
    public void shouldMakeLotteryChoose()
    {
        int positionOfLottery = 10;

        model.newPositionOFPlayer(playerList.get(0), positionOfLottery);

        assertThat(model.shouldMakeLotteryChoose(playerList.get(0)), equalTo(true));

    }

    @Test
    public void isGameContinue()
    {
        for (Player player : playerList)
        {
            player.setMoney(0);
        }

        assertThat(model.isGameContinue().getIsGameContinue(), equalTo(false));
    }

    @Test
    public void throwDice()
    {
        assertThat(model.throwDice(), lessThan(7));
    }

    @Test
    public void newPositionOFPlayer()
    {
        model.newPositionOFPlayer(playerList.get(0), 3);

        StateOfGame state = model.isGameContinue();

        assertThat(state.getPositionsOfPlayers().get(playerList.get(0)).getIdCard(), equalTo(3));
    }

    @Test
    public void buyBuilding()
    {
        int moneyBefore = playerList.get(0).getMoney();
        int positionOfBuilding = 3;
        model.newPositionOFPlayer(playerList.get(0), positionOfBuilding);
        model.buyBuilding(playerList.get(0), true);

        int cost = model.isGameContinue().getPositionOfCards().get(positionOfBuilding).getBuilding().getCost();

        assertThat(moneyBefore - cost, equalTo(playerList.get(0).getMoney()));
    }

    @Test
    public void shouldPayAnotherPlayer()
    {
        int positionOfBuilding = 3;
        model.newPositionOFPlayer(playerList.get(1), positionOfBuilding);
        model.buyBuilding(playerList.get(1), true);

        int moneyBefore = playerList.get(0).getMoney();
        model.newPositionOFPlayer(playerList.get(0), positionOfBuilding);
        model.shouldMakeBuyingChoose(playerList.get(0));
        int tax = model.isGameContinue().getPositionOfCards().get(positionOfBuilding).getBuilding().getTax();

        assertThat(playerList.get(0).getMoney(), equalTo(moneyBefore-tax));
    }

    @Test
    public void tryLotteryPlus()
    {
        int moneyBefore = playerList.get(0).getMoney();
        int result = 5;

        model.tryLottery(playerList.get(0), result);
        assertThat(playerList.get(0).getMoney(), greaterThan(moneyBefore));
    }

    @Test
    public void tryLotteryMinus()
    {
        int moneyBefore = playerList.get(0).getMoney();
        int result = 3;

        model.tryLottery(playerList.get(0), result);
        assertThat(playerList.get(0).getMoney(), lessThan(moneyBefore));
    }

    @Test
    public void getStateOfGame()
    {
        model.setStateOfGame(new StateOfGame(playerList, cards));
        assertThat(model.getStateOfGame(), notNullValue());
    }

    @Test
    public void notBuisnessBuilding()
    {
        int position = 0;

        model.newPositionOFPlayer(playerList.get(0), position);
        assertThat(model.shouldMakeBuyingChoose(playerList.get(0)), equalTo(false));
    }

    @Test
    public void playerDontWantToBuy()
    {
        int position = 3;

        model.newPositionOFPlayer(playerList.get(3), position);
        model.buyBuilding(playerList.get(0), false);
        assertThat(model.shouldMakeBuyingChoose(playerList.get(0)), equalTo(false));
    }

    @Test
    public void playerGoThrowStart()
    {
        int moneyBeforeStart = playerList.get(0).getMoney();

        model.newPositionOFPlayer(playerList.get(0), 0);
        assertThat(playerList.get(0).getMoney(), greaterThan(moneyBeforeStart));
    }

    @Test
    public void playerMinus()
    {
        int posOfMinus = 4;

        int moneyBefore = playerList.get(0).getMoney();
        model.newPositionOFPlayer(playerList.get(0), posOfMinus);
        assertThat(playerList.get(0).getMoney(), lessThan(moneyBefore));
    }

    @Test
    public void playerPlus()
    {
        int posOfPlus = 6;

        int moneyBefore = playerList.get(0).getMoney();
        model.newPositionOFPlayer(playerList.get(0), posOfPlus);
        assertThat(playerList.get(0).getMoney(), greaterThan(moneyBefore));
    }

    @Test
    public void payMoneyWithoutNeg()
    {
        int positionOfBuilding = 3;
        model.newPositionOFPlayer(playerList.get(1), positionOfBuilding);
        model.buyBuilding(playerList.get(1), true);

        playerList.get(0).setMoney(10);
        model.newPositionOFPlayer(playerList.get(0), positionOfBuilding);
        model.shouldMakeBuyingChoose(playerList.get(0));
        assertThat(playerList.get(0).getMoney(), equalTo(0));
    }

    @Test
    public void tryToBuyWithoutMoney()
    {
        int moneyBefore = playerList.get(0).getMoney();
        int positionOfBuilding = 3;
        playerList.get(0).setMoney(0);
        model.newPositionOFPlayer(playerList.get(0), positionOfBuilding);
        model.buyBuilding(playerList.get(0), true);
        assertThat(0, equalTo(playerList.get(0).getMoney()));
    }
}