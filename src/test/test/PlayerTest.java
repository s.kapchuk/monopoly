package test.test;

import com.company.controller.Player;
import com.company.model.StateOfGameEntity;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlayerTest
{
    private Player player = new Player();

    @org.junit.Test
    public void getName()
    {
        String name = "Anna";
        player.setName(name);
        assertThat(name, equalTo(player.getName()));
    }

    @org.junit.Test
    public void getMoney()
    {
        int money = 100;
        player.setMoney(money);
        assertThat(money, equalTo(player.getMoney()));
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setMoney()
    {
        player.setMoney(-200);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setNameNull()
    {
        player.setName(null);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setName()
    {
        player.setName("");
    }

    @Test
    public void setShiftX()
    {
        player.setShiftX(2);
        assertThat(player.getShiftX(), equalTo(2));
    }

    @Test
    public void setStateOfGameEntity()
    {
        StateOfGameEntity entity = Mockito.mock(StateOfGameEntity.class);
        Mockito.when(entity.getId()).thenReturn((long)1);

        player.setStateOfGameEntity(entity);
        assertThat(entity.getId(), equalTo(player.getId_stateOfGameEntity()));
    }

    @Test
    public void getIsRealPlayer()
    {
        player.setIsRealPlayer(false);
        assertThat(player.getIsRealPlayer(), equalTo(false));
    }

    @Test
    public void setId_stateOfGameEntity()
    {
        long id = 1;
        player.setId_stateOfGameEntity(id);
        assertThat(player.getId_stateOfGameEntity(), equalTo(id));
    }

    @Test
    public void checkCompare()
    {
        Player playerComp = Mockito.mock(Player.class);
        Mockito.when(playerComp.getShiftX()).thenReturn(2);

        player.setShiftX(2);
        assertThat(player.compareTo(playerComp), equalTo(0));
    }

    @Test
    public void hasId()
    {
        assertThat(player.hasId(), equalTo(false));
    }
}