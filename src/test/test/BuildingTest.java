package test.test;

import com.company.controller.Building;
import com.company.controller.Player;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BuildingTest
{

    private Building building = new Building();

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCost()
    {
        building.setCost(-2);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setTax()
    {
        building.setTax(-2);
    }

    @org.junit.Test
    public void getCost()
    {
        building.setCost(100);
        assertThat(100, equalTo(building.getCost()));
    }

    @org.junit.Test
    public void getTax()
    {
        int tax = 10;
        building.setTax(tax);
        assertThat(tax, equalTo(building.getTax()));
    }

    @org.junit.Test
    public void getName()
    {
        String name = "Building";
        building.setName(name);
        assertThat(name, equalTo(building.getName()));
    }

    @org.junit.Test
    public void getOwner()
    {
        Player owner = new Player();
        building.setOwner(owner);
        assertThat(owner, equalTo(building.getOwner()));
    }

    @org.junit.Test
    public void isHasOwnerTrue()
    {
        Player owner = new Player();
        building.setOwner(owner);
        assertThat(true, equalTo(building.isHasOwner()));
    }

    @org.junit.Test
    public void isHasOwnerFalse()
    {
        Player owner = null;
        building.setOwner(owner);
        assertThat(false, equalTo(building.isHasOwner()));
    }

    @Test
    public void hasId()
    {
        assertThat(building.hasId(), equalTo(false));
    }
}