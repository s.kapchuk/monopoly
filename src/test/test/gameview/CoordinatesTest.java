package test.test.gameview;

import com.company.gameview.icons.Coordinates;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CoordinatesTest
{
    private Coordinates coordinates = new Coordinates(1, 1);

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void createCoordinatesCheckX()
    {
        coordinates = new Coordinates(-2, 0);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void createCoordinatesCheckY()
    {
        coordinates = new Coordinates(1, -4);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void createCoordinatesCheckXY()
    {
        coordinates = new Coordinates(-2, -1);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setX()
    {
        coordinates.setX(-2);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setY()
    {
        coordinates.setY(-1);
    }

    @org.junit.Test
    public void updateX()
    {
        int stepX = 10;
        int value = coordinates.getX();
        coordinates.updateX(stepX);
        value += stepX;
        assertThat(value, equalTo(coordinates.getX()));
    }

    @org.junit.Test
    public void updateY()
    {
        int stepY = 10;
        int value = coordinates.getY();
        coordinates.updateY(stepY);
        value += stepY;
        assertThat(value, equalTo(coordinates.getY()));
    }

    @org.junit.Test
    public void getX()
    {
        int x = 20;

        coordinates.setX(x);
        assertThat(x, equalTo(coordinates.getX()));
    }

    @org.junit.Test
    public void getY()
    {
        int y = 20;

        coordinates.setY(y);
        assertThat(y, equalTo(coordinates.getY()));
    }

}