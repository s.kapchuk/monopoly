package gameview;

import com.company.gameview.GamePanelSizes;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

public class GamePanelSizesTest
{

    private GamePanelSizes gamePanelSizes;
    private int width = 1000;
    private int height = 1000;
    private int numberOfCards = 6;

    @Before
    public void init()
    {
        gamePanelSizes = new GamePanelSizes(width, height, numberOfCards);

    }

    @Test
    public void getNameIndentX()
    {
        assertThat(gamePanelSizes.getNameIndentX(), equalTo(gamePanelSizes.getBuildingIndentX()));
    }

    @Test
    public void getNameIndentY()
    {
        assertThat(gamePanelSizes.getNameIndentY(), lessThan(gamePanelSizes.getBuildingIndentY()));
    }

    @Test
    public void getCostIndentY()
    {
        assertThat(gamePanelSizes.getCostIndentY(), greaterThan(gamePanelSizes.getNameIndentY()));
    }

    @Test
    public void getTaxIndentY()
    {
        assertThat(gamePanelSizes.getTaxIndentY(), greaterThan(gamePanelSizes.getCostIndentY()));
    }

    @Test
    public void getOwnerIndentY()
    {
        assertThat(gamePanelSizes.getOwnerIndentY(), greaterThan(gamePanelSizes.getTaxIndentY()));
    }

    @Test
    public void getChipHeight()
    {
        assertThat(gamePanelSizes.getChipHeight(), lessThan(gamePanelSizes.getCardHeight()));
    }

    @Test
    public void getChipWidth()
    {
        assertThat(gamePanelSizes.getChipWidth(), lessThan(gamePanelSizes.getCardWidth()));
    }

    @Test
    public void getChipIndentY()
    {
        assertThat(gamePanelSizes.getChipIndentY(), greaterThan(gamePanelSizes.getBuildingIndentY()));
    }

    @Test
    public void getChipIndentX()
    {
        assertThat(gamePanelSizes.getChipIndentX(), greaterThan(gamePanelSizes.getBuildingIndentX()));
    }

    @Test
    public void getCardWidth()
    {
        int expectedWidth = width/numberOfCards;

        assertThat(expectedWidth, equalTo(gamePanelSizes.getCardWidth()));
    }

    @Test
    public void getCardHeight()
    {
        int expectedHeight = (height-35)/numberOfCards;

        assertThat(expectedHeight, equalTo(gamePanelSizes.getCardHeight()));
    }

   @Test
    public void getCardBuildingWidth()
    {
        assertThat(gamePanelSizes.getCardBuildingWidth(), lessThan(width/numberOfCards));
    }

    @Test
    public void getCardBuildingHeight()
    {
        assertThat(gamePanelSizes.getCardBuildingHeight(), lessThan(height/numberOfCards));
    }

    @Test
    public void getRecordIndentX()
    {
        assertThat(gamePanelSizes.getRecordIndentX(), lessThan(width/numberOfCards));
    }

    @Test
    public void getBuildingIndentX()
    {
        assertThat(gamePanelSizes.getBuildingIndentX(), equalTo((int)(gamePanelSizes.getCardWidth()*0.1)));
    }

    @Test
    public void getBuildingIndentY()
    {
        assertThat(gamePanelSizes.getBuildingIndentY(), equalTo((int)(gamePanelSizes.getCardHeight()*0.3)));
    }
}