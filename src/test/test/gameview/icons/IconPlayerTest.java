package test.test.gameview.icons;

import com.company.controller.Player;
import com.company.gameview.icons.Coordinates;
import com.company.gameview.icons.IconPlayer;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IconPlayerTest
{
    private byte[] byteArray = new byte[]{0, 0, 1};
    private IconPlayer iconPlayer = new IconPlayer(byteArray);

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setPlayer()
    {
        iconPlayer.setPlayer(null);
    }

    @org.junit.Test
    public void getPlayer()
    {
        Player player = new Player();
        iconPlayer.setPlayer(player);
        assertThat(player, equalTo(iconPlayer.getPlayer()));
    }

    @org.junit.Test
    public void getCoordinates()
    {
        Coordinates coordinates = new Coordinates(3, 4);
        iconPlayer.setCoordinates(coordinates);

        assertThat(coordinates, equalTo(iconPlayer.getCoordinates()));
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCoordinates()
    {
        iconPlayer.setCoordinates(null);
    }
}