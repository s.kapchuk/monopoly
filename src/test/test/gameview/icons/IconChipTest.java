package test.test.gameview.icons;

import com.company.gameview.icons.Coordinates;
import com.company.gameview.icons.IconChip;
import org.junit.Test;

import javax.swing.*;

import java.awt.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IconChipTest
{

    private byte[] byteArray = new byte[]{0, 0, 1};
    private IconChip iconChip = new IconChip(0, byteArray);

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void createIconChip()
    {
        iconChip = new IconChip(-2, byteArray);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCoordinates()
    {
        iconChip.setCoordinates(null);
    }

    @org.junit.Test
    public void getCoordinates()
    {
        Coordinates coordinates = new Coordinates(1, 1);
        iconChip.setCoordinates(coordinates);

        assertThat(coordinates, equalTo(iconChip.getCoordinates()));
    }

    @Test
    public void getIdCard()
    {
        iconChip.setIdCard(2);
        assertThat(iconChip.getIdCard(), equalTo(2));
    }
}