package test.test.gameview.icons;

import com.company.controller.Building;
import com.company.gameview.icons.IconBuilding;
import org.mockito.Mockito;

import javax.swing.*;
import java.awt.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IconBuildingTest
{
    private byte[] byteArray = new byte[]{0, 0, 1};
    private IconBuilding iconBuilding = new IconBuilding(1, byteArray);

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void createIconBuilding()
    {
        iconBuilding = new IconBuilding(-1, byteArray);
    }

    @org.junit.Test
    public void getBuilding()
    {
       Building building = new Building();

       iconBuilding.setBuilding(building);
       assertThat(building, equalTo(iconBuilding.getBuilding()));
    }
}