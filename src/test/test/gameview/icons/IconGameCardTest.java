package test.test.gameview.icons;

import com.company.controller.Building;
import com.company.controller.BuildingType;
import com.company.controller.Card;
import com.company.gameview.icons.Coordinates;
import com.company.gameview.icons.IconGameCard;

import javax.swing.*;
import java.awt.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IconGameCardTest
{
    private byte[] byteArray = new byte[]{0, 0, 1};
    private IconGameCard iconGameCard = new IconGameCard(byteArray);

    @org.junit.Test
    public void getCard()
    {
        Card card = new Card().setBuilding(new Building().setBuildingType(BuildingType.BUSINESS));

        iconGameCard.setCard(card, byteArray, byteArray, byteArray, byteArray);
        assertThat(card, equalTo(iconGameCard.getCard()));
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCard()
    {
        iconGameCard.setCard(null, byteArray, byteArray, byteArray, byteArray);
    }

    @org.junit.Test
    public void getCoordinates()
    {
        Coordinates coordinates = new Coordinates(2, 2);

        iconGameCard.setCoordinates(coordinates);
        assertThat(coordinates, equalTo(iconGameCard.getCoordinates()));
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCoordinates()
    {
        iconGameCard.setCoordinates(null);
    }

    @org.junit.Test
    public void getIconBuilding()
    {
        Card card = new Card();
        Building building = new Building().setBuildingType(BuildingType.BUSINESS);
        card.setBuilding(building).setIdCard(1);
        iconGameCard.setCard(card, byteArray, byteArray, byteArray, byteArray);

        assertThat(building, equalTo(iconGameCard.getIconBuilding().getBuilding()));
    }

    @org.junit.Test
    public void isHasIconBuildingTrue()
    {
        assertThat(false, equalTo(iconGameCard.isHasIconBuilding()));
    }

    @org.junit.Test
    public void isHasIconBuildingFalse()
    {
        Card card = new Card();
        iconGameCard.setCard(card.setBuilding(new Building().setBuildingType(BuildingType.POLICE)).setIdCard(1), byteArray, byteArray, byteArray, byteArray);

        assertThat(true, equalTo(iconGameCard.isHasIconBuilding()));
    }
}